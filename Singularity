Bootstrap: docker
From: ubuntu:bionic

%post
    ##### INSTALLATION DEPENDENCIES #####
    ## Install all necessary dependencies
    # We install only those boost packages that required by the project to reduce the size of the image.
    apt-get update
    apt-get install --no-install-recommends -y \
            build-essential \
            pkg-config \
            g++ \
            python3 python3-pyparsing \
            git \
            scons \
            less \
            gdb \
            libboost-program-options-dev libboost-filesystem-dev libboost-system-dev \
            libboost-chrono-dev libboost-timer-dev libboost-serialization-dev \
            ca-certificates
    rm -rf /var/lib/apt/lists/*

    git clone -b ipc-2018-seq-sat https://bitbucket.org/ipc2018-classical/team36.git planning/fs-planner

    ##### CLINGO INSTALLATION #####
    ## At the moment we simply download the 64-bit binaries and create and appropriate
    ## link to them which will be used afterwards as part of an environment variable
    #cd /planning
    #curl -SL http://github.com/potassco/clingo/releases/download/v5.2.2/clingo-5.2.2-linux-x86_64.tar.gz | tar xz \
    #    && ln -s clingo-5.2.2-linux-x86_64 clingo
    cd /planning
    tar xfz /planning/fs-planner/ext/clingo-5.2.2-linux-x86_64.tar.gz \
        && rm -rf clingo && ln -s clingo-5.2.2-linux-x86_64 clingo


    ##### GECODE INSTALLATION #####
    ## We install from source rather than use the package to minimize the bloat,
    ## since the official package installs by default all Gecode modules, including visualization tools, etc.
    ## We configure the build with only the strictly required modules
    cd /tmp
    tar xfz /planning/fs-planner/ext/gecode-5.1.0.tar.gz \
        && cd gecode-5.1.0 \
        && CXXFLAGS="-Wno-unused-variable -Wno-unused-parameter" ./configure \
        --disable-minimodel \
        --disable-examples \
        --disable-flatzinc \
        --disable-gist \
        --disable-driver \
        --disable-qt \
        --disable-mpfr \
        --disable-doc-tagfile \
        --disable-doc-dot \
        --disable-thread \
        && make -j6 && make install


    ##### PLANNER INSTALLATION #####
    ## Compile the planner
    cd /planning/fs-planner
    python ./build.py -p

%environment
    ## Set the appropriate library path for Gecode to be found
    LD_LIBRARY_PATH=/usr/local/lib/:$LD_LIBRARY_PATH
    GRINGO_PATH=/planning/clingo


%runscript
    ## Set the appropriate library path for Gecode to be found
    export LD_LIBRARY_PATH=/usr/local/lib/:$LD_LIBRARY_PATH
    export GRINGO_PATH=/planning/clingo

    DOMAINFILE=$1
    PROBLEMFILE=$2
    # If an absolute path was passed, use it directly
    if [ "$3" != "${3#/}" ]
        then PLANFILE=$3;
    else
        PLANFILE=`pwd`/$3;
    fi

    WORKSPACE=~/workspace

    mkdir -p ${WORKSPACE}

    /planning/fs-planner/run.py --asp -i ${PROBLEMFILE} --domain ${DOMAINFILE} \
               --planfile ${PLANFILE} \
               --driver sbfws --options "successor_generation=adaptive,evaluator_t=adaptive,bfws.rs=sim,sim.r_g_prime=true,width.simulation=2,sim.act_cutoff=40000" \
               --workspace ${WORKSPACE}


%labels
## Update the following fields with meta data about your submission.
## Please use the same field names and use only one line for each value.
Name        fs-sim
Description TODO
Authors     Guillem Francès <guillem.frances@unibas.ch>, Miquel Ramírez <miguel.ramirez@unimelb.edu.au>, Nir Lipovetzky <nir.lipovetzky@unimelb.edu.au>, Héctor Geffner <hector.geffner@upf.edu>
SupportsDerivedPredicates no
SupportsQuantifiedPreconditions yes
SupportsQuantifiedEffects yes
